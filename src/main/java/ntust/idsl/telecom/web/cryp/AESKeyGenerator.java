package ntust.idsl.telecom.web.cryp;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.security.NoSuchAlgorithmException;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

import ntust.idsl.telecom.web.util.FileNameGenerator;

/**
 * @author Carl Adler (C.A.)
 * */
public class AESKeyGenerator {
	public static byte[] generateSecretKey() {
		KeyGenerator keyGenerator;
		try {
			keyGenerator = KeyGenerator.getInstance("AES");
			keyGenerator.init(128);
			SecretKey secretKey = keyGenerator.generateKey();
			byte[] key = secretKey.getEncoded();
			return key;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static void writeKeyToFile(byte[] key) {
		try {
			ByteArrayInputStream arrayInputStream = new ByteArrayInputStream(key);
			ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
			FileOutputStream out = new FileOutputStream("key.properties");
			int c = arrayInputStream.read();
			while(c != -1) {
				arrayOutputStream.write(c);
				c = arrayInputStream.read();
			}
		arrayOutputStream.writeTo(out);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static byte[] obtainKeyFromFile() {
		try {
			File file = new File(FileNameGenerator.location + "\\key.properties");
			BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(file));
			ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
			byte[] bytes = new byte[1];
			while(bufferedInputStream.read(bytes) != -1) {
				arrayOutputStream.write(bytes);
			}
			arrayOutputStream.close();
			bufferedInputStream.close();
			return arrayOutputStream.toByteArray();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
