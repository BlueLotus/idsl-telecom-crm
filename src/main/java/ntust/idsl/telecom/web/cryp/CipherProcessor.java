package ntust.idsl.telecom.web.cryp;

/**
 * @author Carl Adler(C.A.)
 * */
public class CipherProcessor {
	
	private byte[] secretKey;
	private String initialVector;
	
	public CipherProcessor() {
		try {
			System.out.println("Cipher material initializing...\n");
			secretKey = AESKeyGenerator.obtainKeyFromFile();
			initialVector = "3102843462069914";
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String encryptMessage(String message) {
		System.out.println("	Message to be encrypted: " + message);
		System.out.println("	Secret Key: " + new String(secretKey));
		String encryptedMessage = AESEncryptor.encryptForStringWithCBCMode(message, secretKey, initialVector);
		System.out.println("	Message after encrypted: " + encryptedMessage + "\n");
		return encryptedMessage;
	}
	
}
