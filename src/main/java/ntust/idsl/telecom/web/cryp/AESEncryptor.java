package ntust.idsl.telecom.web.cryp;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

/**
 * @author Carl Adler (C.A.)
 * */
public class AESEncryptor {
	
	private static Cipher cipher;
	private static SecretKeySpec secretKey;

	public static String encryptForStringWithECBMode(String strToEncrypt, byte[] key) {
		try {
			cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
			secretKey = new SecretKeySpec(key, "AES");
			cipher.init(Cipher.ENCRYPT_MODE, secretKey);
			return Base64.encodeBase64String(cipher.doFinal(strToEncrypt.getBytes("UTF-8")));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static String encryptForStringWithCBCMode(String strToEncrypt, byte[] key, String initialVector) {
		try {
			cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
			secretKey = new SecretKeySpec(key, "AES");
			IvParameterSpec iv = new IvParameterSpec(initialVector.getBytes());
			cipher.init(Cipher.ENCRYPT_MODE, secretKey, iv);
			return Base64.encodeBase64String(cipher.doFinal(strToEncrypt.getBytes("UTF-8")));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
