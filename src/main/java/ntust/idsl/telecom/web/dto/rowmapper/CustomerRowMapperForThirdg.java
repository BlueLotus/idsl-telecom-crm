package ntust.idsl.telecom.web.dto.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import ntust.idsl.telecom.web.dto.Customer;
import ntust.idsl.telecom.web.util.DateHandler;

import org.springframework.jdbc.core.RowMapper;

/**
 * @author Carl Adler(C.A.)
 * */
public class CustomerRowMapperForThirdg implements RowMapper<Customer> {

	@Override
	public Customer mapRow(ResultSet rs, int rowNum) throws SQLException {
		Customer customer = new Customer();
		customer.setCustomerId(String.valueOf(rs.getInt("customerID")));
		customer.setCustomerName(rs.getString("customerName"));
		customer.setAge(String.valueOf(rs.getInt("age")));
		customer.setGender(rs.getString("gender"));
		customer.setBirthday(DateHandler.getStringDateFromDateObj(rs.getDate("birthday")));
		customer.setAddress(rs.getString("address"));
		customer.setContactPhone(rs.getString("contactPhone"));
		customer.setMdnForThirdg(rs.getString("thirdgMdn"));
		customer.setProvStatusForThirdg(rs.getString("provStatus"));
		customer.setVoiceRateIdForThirdg(rs.getInt("voiceRateID"));
		customer.setVoiceFeeForThirdg(rs.getInt("voice_fee"));
		customer.setMobileRateIdForThirdg(rs.getInt("mobileRateID"));
		customer.setMobileFeeForThirdg(rs.getInt("mobile_fee"));
		customer.setContractTypeForThirdg(rs.getString("contractType"));
		customer.setEffectedDateForThirdg(DateHandler.getStringDateFromDateObj(rs.getDate("effectedDate")));
		customer.setTerminationDateForThirdg(DateHandler.getStringDateFromDateObj(rs.getDate("terminationDate")));
		return customer;
	}
	
}
