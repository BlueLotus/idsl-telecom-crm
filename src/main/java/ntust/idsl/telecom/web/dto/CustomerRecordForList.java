package ntust.idsl.telecom.web.dto;

import java.io.Serializable;

/**
 * @author Carl Adler(C.A.)
 * */
public class CustomerRecordForList implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private int customerId;
	private String customerName;
	private String mdn;
	private int voiceFee;
	private int mobileFee;
	private String contractType;
	private String provStatus;
	private String effectedDate;
	private String terminationDate;
	
	public int getCustomerId() {
		return customerId;
	}
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	
	public String getMdn() {
		return mdn;
	}
	public void setMdn(String mdn) {
		this.mdn = mdn;
	}
	
	public int getVoiceFee() {
		return voiceFee;
	}
	public void setVoiceFee(int voiceFee) {
		this.voiceFee = voiceFee;
	}
	
	public int getMobileFee() {
		return mobileFee;
	}
	public void setMobileFee(int mobileFee) {
		this.mobileFee = mobileFee;
	}
	
	public String getContractType() {
		return contractType;
	}
	public void setContractType(String contractType) {
		this.contractType = contractType;
	}
	
	public String getProvStatus() {
		return provStatus;
	}
	public void setProvStatus(String provStatus) {
		this.provStatus = provStatus;
	}
	
	public String getEffectedDate() {
		return effectedDate;
	}
	public void setEffectedDate(String effectedDate) {
		this.effectedDate = effectedDate;
	}
	
	public String getTerminationDate() {
		return terminationDate;
	}
	public void setTerminationDate(String terminationDate) {
		this.terminationDate = terminationDate;
	}
	
}
