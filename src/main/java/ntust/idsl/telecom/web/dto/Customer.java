package ntust.idsl.telecom.web.dto;

import java.io.Serializable;

/**
 * @author Carl Adler(C.A.)
 * */
public class Customer implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String customerId;
	private String customerName;
	private String age;
	private String gender;
	private String birthday;
	private String address;
	private String contactPhone;
	
	private String mdnForThirdg;
	private String provStatusForThirdg;
	private int voiceRateIdForThirdg;
	private int voiceFeeForThirdg;
	private int mobileRateIdForThirdg;
	private int mobileFeeForThirdg;
	private String contractTypeForThirdg;
	private String effectedDateForThirdg;
	private String terminationDateForThirdg;
	
	private String mdnForFourthg;
	private String provStatusForFourthg;
	private int voiceRateIdForFourthg;
	private int voiceFeeForFourthg;
	private int mobileRateIdForFourthg;
	private int mobileFeeForFourthg;
	private String contractTypeForFourthg;
	private String effectedDateForFourthg;
	private String terminationDateForFourthg;
	
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getContactPhone() {
		return contactPhone;
	}
	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}
	
	public String getMdnForThirdg() {
		return mdnForThirdg;
	}
	public void setMdnForThirdg(String mdnForThirdg) {
		this.mdnForThirdg = mdnForThirdg;
	}
	
	public String getProvStatusForThirdg() {
		return provStatusForThirdg;
	}
	public void setProvStatusForThirdg(String provStatusForThirdg) {
		this.provStatusForThirdg = provStatusForThirdg;
	}
	
	public int getVoiceRateIdForThirdg() {
		return voiceRateIdForThirdg;
	}
	public void setVoiceRateIdForThirdg(int voiceRateIdForThirdg) {
		this.voiceRateIdForThirdg = voiceRateIdForThirdg;
	}
	
	public int getVoiceFeeForThirdg() {
		return voiceFeeForThirdg;
	}
	public void setVoiceFeeForThirdg(int voiceFeeForThirdg) {
		this.voiceFeeForThirdg = voiceFeeForThirdg;
	}
	
	public int getMobileRateIdForThirdg() {
		return mobileRateIdForThirdg;
	}
	public void setMobileRateIdForThirdg(int mobileRateIdForThirdg) {
		this.mobileRateIdForThirdg = mobileRateIdForThirdg;
	}
	
	public int getMobileFeeForThirdg() {
		return mobileFeeForThirdg;
	}
	public void setMobileFeeForThirdg(int mobileFeeForThirdg) {
		this.mobileFeeForThirdg = mobileFeeForThirdg;
	}
	
	public String getContractTypeForThirdg() {
		return contractTypeForThirdg;
	}
	public void setContractTypeForThirdg(String contractTypeForThirdg) {
		this.contractTypeForThirdg = contractTypeForThirdg;
	}
	
	public String getEffectedDateForThirdg() {
		return effectedDateForThirdg;
	}
	public void setEffectedDateForThirdg(String effectedDateForThirdg) {
		this.effectedDateForThirdg = effectedDateForThirdg;
	}
	
	public String getTerminationDateForThirdg() {
		return terminationDateForThirdg;
	}
	public void setTerminationDateForThirdg(String terminationDateForThirdg) {
		this.terminationDateForThirdg = terminationDateForThirdg;
	}
	
	public String getMdnForFourthg() {
		return mdnForFourthg;
	}
	public void setMdnForFourthg(String mdnForFourthg) {
		this.mdnForFourthg = mdnForFourthg;
	}
	
	public String getProvStatusForFourthg() {
		return provStatusForFourthg;
	}
	public void setProvStatusForFourthg(String provStatusForFourthg) {
		this.provStatusForFourthg = provStatusForFourthg;
	}
	
	public int getVoiceRateIdForFourthg() {
		return voiceRateIdForFourthg;
	}
	public void setVoiceRateIdForFourthg(int voiceRateIdForFourthg) {
		this.voiceRateIdForFourthg = voiceRateIdForFourthg;
	}
	
	public int getVoiceFeeForFourthg() {
		return voiceFeeForFourthg;
	}
	public void setVoiceFeeForFourthg(int voiceFeeForFourthg) {
		this.voiceFeeForFourthg = voiceFeeForFourthg;
	}
	
	public int getMobileRateIdForFourthg() {
		return mobileRateIdForFourthg;
	}
	public void setMobileRateIdForFourthg(int mobileRateIdForFourthg) {
		this.mobileRateIdForFourthg = mobileRateIdForFourthg;
	}
	
	public int getMobileFeeForFourthg() {
		return mobileFeeForFourthg;
	}
	public void setMobileFeeForFourthg(int mobileFeeForFourthg) {
		this.mobileFeeForFourthg = mobileFeeForFourthg;
	}
	
	public String getContractTypeForFourthg() {
		return contractTypeForFourthg;
	}
	public void setContractTypeForFourthg(String contractTypeForFourthg) {
		this.contractTypeForFourthg = contractTypeForFourthg;
	}
	
	public String getEffectedDateForFourthg() {
		return effectedDateForFourthg;
	}
	public void setEffectedDateForFourthg(String effectedDateForFourthg) {
		this.effectedDateForFourthg = effectedDateForFourthg;
	}
	
	public String getTerminationDateForFourthg() {
		return terminationDateForFourthg;
	}
	public void setTerminationDateForFourthg(String terminationDateForFourthg) {
		this.terminationDateForFourthg = terminationDateForFourthg;
	}
	
}
