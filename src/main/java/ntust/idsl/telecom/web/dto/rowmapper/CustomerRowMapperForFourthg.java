package ntust.idsl.telecom.web.dto.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import ntust.idsl.telecom.web.dto.Customer;
import ntust.idsl.telecom.web.util.DateHandler;

import org.springframework.jdbc.core.RowMapper;

/**
 * @author Carl Adler(C.A.)
 * */
public class CustomerRowMapperForFourthg implements RowMapper<Customer> {

	@Override
	public Customer mapRow(ResultSet rs, int rowNum) throws SQLException {
		Customer customer = new Customer();
		customer.setCustomerId(String.valueOf(rs.getInt("customerID")));
		customer.setCustomerName(rs.getString("customerName"));
		customer.setAge(String.valueOf(rs.getInt("age")));
		customer.setGender(rs.getString("gender"));
		customer.setBirthday(DateHandler.getStringDateFromDateObj(rs.getDate("birthday")));
		customer.setAddress(rs.getString("address"));
		customer.setContactPhone(rs.getString("contactPhone"));
		customer.setMdnForFourthg(rs.getString("fourthgMdn"));
		customer.setProvStatusForFourthg(rs.getString("provStatus"));
		customer.setVoiceRateIdForFourthg(rs.getInt("voiceRateID"));
		customer.setVoiceFeeForFourthg(rs.getInt("voice_fee"));
		customer.setMobileRateIdForFourthg(rs.getInt("mobileRateID"));
		customer.setMobileFeeForFourthg(rs.getInt("mobile_fee"));
		customer.setContractTypeForFourthg(rs.getString("contractType"));
		customer.setEffectedDateForFourthg(DateHandler.getStringDateFromDateObj(rs.getDate("effectedDate")));
		customer.setTerminationDateForFourthg(DateHandler.getStringDateFromDateObj(rs.getDate("terminationDate")));
		return customer;
	}
	
}
