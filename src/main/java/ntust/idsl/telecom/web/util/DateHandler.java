package ntust.idsl.telecom.web.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateHandler {
	
	private static String dateFormat = "yyyy/MM/dd";
	private static DateFormat formatter = new SimpleDateFormat(dateFormat);
	public static Date getDateWithString(String inputDate) {
		Date date = null;
		try {
			date = formatter.parse(inputDate);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return date;
	}
	
	public static String getStringDateFromDateObj(Date date) {
		return formatter.format(date);
	}
	
}
