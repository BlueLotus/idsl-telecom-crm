package ntust.idsl.telecom.web.util;

/**
 * @author Carl Adler (C.A.)
 * */
public class FileNameGenerator {
	public static String location = "C:\\WorkSpace\\IDSL-Telecom\\idsl-telecom-crm";
	public static String PRIVATE_KEY_FOR_CLIENT = location + "\\privateKeyForClient.key";
	public static String PRIVATE_KEY_FOR_SERVER = location + "\\privateKeyForServer.key";
	public static String PUBLIC_KEY_FOR_CLIENT = location +"\\publicKeyForClient.key";
	public static String PUBLIC_KEY_FOR_SERVER = location +"\\publicKeyForServer.key";
	public static String SIGNATRUE_FOR_CLIENT = location +"\\signatureForClient.sig";
	public static String SIGNATURE_FOR_SERVER = location +"\\signatureForServer.sig";

}
