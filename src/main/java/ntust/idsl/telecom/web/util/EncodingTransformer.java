package ntust.idsl.telecom.web.util;

import java.io.UnsupportedEncodingException;

/**
 * <p>
 * Since the Apache Tomcat has default encoding "ISO-8859-1" for the POST method,
 * so when using chinese, there will encounter some encoding problem, this class
 * can resolve this problem with method "transformEncoding".
 * </p>
 * @author Carl Adler(C.A.)
 * */
public class EncodingTransformer {
	
	final private static String DEFAULT_ENCODING_FOR_POST_METHOD_IN_TOMCAT = "ISO-8859-1";
	final private static String NEW_ENCODING = "UTF-8";
	
	public static String transformEncoding(String str) throws UnsupportedEncodingException {
		return new String(str.getBytes(DEFAULT_ENCODING_FOR_POST_METHOD_IN_TOMCAT), NEW_ENCODING);
	}
}
