package ntust.idsl.telecom.web.util;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import ntust.idsl.telecom.web.cryp.CipherProcessor;
import ntust.idsl.telecom.web.dto.Customer;
import ntust.idsl.telecom.web.stub.thirdg.ThirdgServiceImplStub.CustomerRecord;

/**
 * @author Carl Adler(C.A.)
 * */
public class CustomerDataTransformer {
	
	private static final Logger logger = Logger.getLogger(CustomerDataTransformer.class);
	
	public static CustomerRecord transformCustomerDataForThirdGProvision(Customer customer) {
		CipherProcessor cipherProcessor = new CipherProcessor();
		CustomerRecord customerRecord = new CustomerRecord();
		customerRecord.setMdn(cipherProcessor.encryptMessage(customer.getMdnForThirdg()));
		customerRecord.setProvStatus(cipherProcessor.encryptMessage("Activate"));
		customerRecord.setVoiceRateID(customer.getVoiceRateIdForThirdg());
		customerRecord.setMobileRateID(customer.getMobileRateIdForThirdg());
		customerRecord.setEffectedDate(DateHandler.getDateWithString(customer.getEffectedDateForThirdg()));
		customerRecord.setTerminationDate(DateHandler.getDateWithString(customer.getTerminationDateForThirdg()));
		return customerRecord;
	}
	
	public static ntust.idsl.telecom.web.stub.thirdg.ThirdgBackup.CustomerRecord transformCustomerDataForThirdGBackupProvision(Customer customer) {
		ntust.idsl.telecom.web.stub.thirdg.ThirdgBackup.CustomerRecord customerRecord = new ntust.idsl.telecom.web.stub.thirdg.ThirdgBackup.CustomerRecord();
		customerRecord.setMdn(customer.getMdnForThirdg());
		customerRecord.setStatus("Activate");
		customerRecord.setVoiceRateID(customer.getVoiceRateIdForThirdg());
		customerRecord.setMobileRateID(customer.getMobileRateIdForThirdg());
		customerRecord.setEffectedDate(DateHandler.getDateWithString(customer.getEffectedDateForThirdg()));
		customerRecord.setTerminationDate(DateHandler.getDateWithString(customer.getTerminationDateForThirdg()));
		return customerRecord;
	}
	
	public static ntust.idsl.telecom.web.stub.fourthg.FourthgServiceImplStub.CustomerRecord transformCustomerDataForFourthGProvision(Customer customer) {
		ntust.idsl.telecom.web.stub.fourthg.FourthgServiceImplStub.CustomerRecord customerRecord = new ntust.idsl.telecom.web.stub.fourthg.FourthgServiceImplStub.CustomerRecord();
		customerRecord.setMdn(customer.getMdnForFourthg());
		customerRecord.setProvStatus("Activate");
		customerRecord.setVoiceRateID(customer.getVoiceRateIdForFourthg());
		customerRecord.setMobileRateID(customer.getMobileRateIdForFourthg());
		customerRecord.setEffectedDate(DateHandler.getDateWithString(customer.getEffectedDateForFourthg()));
		customerRecord.setTerminationDate(DateHandler.getDateWithString(customer.getTerminationDateForFourthg()));
		return customerRecord;
	}
	
	public  static Customer createCustomerFromRequest(HttpServletRequest request) throws Exception {
		Customer customer = new Customer();
		customer.setCustomerName(EncodingTransformer.transformEncoding(request.getParameter("customerName")));
		customer.setAge(EncodingTransformer.transformEncoding(request.getParameter("age")));
		customer.setGender(EncodingTransformer.transformEncoding(request.getParameter("gender")));
		customer.setBirthday(EncodingTransformer.transformEncoding(request.getParameter("birthday")));
		customer.setAddress(EncodingTransformer.transformEncoding(request.getParameter("address")));
		customer.setContactPhone(EncodingTransformer.transformEncoding(request.getParameter("contactPhone")));
		
		if(request.getParameter("thirdgMdn") != null) {
			logger.debug("Provision for thirdg service...");
			customer.setMdnForThirdg(EncodingTransformer.transformEncoding(request.getParameter("thirdgMdn")));
			customer.setVoiceRateIdForThirdg(Integer.valueOf(EncodingTransformer.transformEncoding(request.getParameter("thirdgVoiceRateId"))));
			customer.setMobileRateIdForThirdg(Integer.valueOf(EncodingTransformer.transformEncoding(request.getParameter("thirdgMobileRateId"))));
			customer.setContractTypeForThirdg(EncodingTransformer.transformEncoding(request.getParameter("thirdgContractType")));
			customer.setEffectedDateForThirdg(EncodingTransformer.transformEncoding(request.getParameter("thirdgEffectedDate")));
			customer.setTerminationDateForThirdg(EncodingTransformer.transformEncoding(request.getParameter("thirdgTerminationDate")));
		} else if(request.getParameter("fourthgMdn") != null) {
			logger.debug("Provision for fourthg service...");
			customer.setMdnForFourthg(EncodingTransformer.transformEncoding(request.getParameter("fourthgMdn")));
			customer.setVoiceRateIdForFourthg(Integer.valueOf(EncodingTransformer.transformEncoding(request.getParameter("fourthgVoiceRateId"))));
			customer.setMobileRateIdForFourthg(Integer.valueOf(EncodingTransformer.transformEncoding(request.getParameter("fourthgMobileRateId"))));
			customer.setContractTypeForFourthg(EncodingTransformer.transformEncoding(request.getParameter("fourthgContractType")));
			customer.setEffectedDateForFourthg(EncodingTransformer.transformEncoding(request.getParameter("fourthgEffectedDate")));
			customer.setTerminationDateForFourthg(EncodingTransformer.transformEncoding(request.getParameter("fourthgTerminationDate")));
		} else {
			throw new Exception("Please specify the correct mdn.");
		}
		return customer;
	}
	
	public static  void printOutCustomerProvisionData(Customer customer) {
		logger.debug("Customer data from request:");
		logger.debug("Customer name: " + customer.getCustomerName());
		logger.debug("Age: " + customer.getAge());
		logger.debug("Gender: " + customer.getGender());
		logger.debug("Birthday: " + customer.getBirthday());
		logger.debug("Address: " + customer.getAddress());
		logger.debug("ContactPhone: " + customer.getContactPhone());
		logger.debug("mdnForThirdg: " + customer.getMdnForThirdg());
		logger.debug("provStatusForThirdg: " + customer.getProvStatusForThirdg());
		logger.debug("voiceRateIdForThirdg: " + customer.getVoiceRateIdForThirdg());
		logger.debug("mobileRateIdForThirdg: " + customer.getMobileRateIdForThirdg());
		logger.debug("contractTypeForThirdg: " + customer.getContractTypeForThirdg());
		logger.debug("effectedDateForThirdg: " + customer.getEffectedDateForThirdg());
		logger.debug("terminationDateForThirdg: " + customer.getTerminationDateForThirdg());
		logger.debug("mdnForFourthg: " + customer.getMdnForFourthg());
		logger.debug("provStatusForFourthg: " + customer.getProvStatusForFourthg());
		logger.debug("voiceRateIdForFourthg: " + customer.getVoiceRateIdForFourthg());
		logger.debug("mobileRateIdForFourthg: " + customer.getMobileRateIdForFourthg());
		logger.debug("contractTypeForFourthg: " + customer.getContractTypeForFourthg());
		logger.debug("effectedDateForFourthg: " + customer.getEffectedDateForFourthg());
		logger.debug("terminationDateForFourthg: " + customer.getTerminationDateForFourthg());
	}
	
}
