package ntust.idsl.telecom.web.controller;

import java.util.*;

import javax.servlet.http.HttpServletRequest;

import ntust.idsl.telecom.web.dao.impl.MemberDao;
import ntust.idsl.telecom.web.dto.Customer;
import ntust.idsl.telecom.web.dto.CustomerRecordForList;
import ntust.idsl.telecom.web.service.CRBTService;
import ntust.idsl.telecom.web.service.FourthgService;
import ntust.idsl.telecom.web.service.ThirdgService;
import ntust.idsl.telecom.web.service.ThirdgServiceBackup;
import ntust.idsl.telecom.web.stub.crbt.MusicServerStub.Music;
import ntust.idsl.telecom.web.util.CustomerDataTransformer;
import ntust.idsl.telecom.web.util.EncodingTransformer;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author Carl Adler(C.A.)
 * */
@Controller
public class RootController {

	private static final Logger logger = Logger.getLogger(RootController.class);
	
	@Autowired
	private ThirdgService thirdgService;
	
	@Autowired
	private FourthgService fourthgService;
	
	@Autowired
	private ThirdgServiceBackup thirdgServiceBackup;
	
	@Autowired
	private CRBTService crbtService;
	
	@Autowired
	private MemberDao memberDao;
	
	private boolean loginSuccessfully = false;
	
	@RequestMapping(value = "/")
	public String loginPage() {
		return "content/login";
	}

	@RequestMapping(value = "/telecom/doLogin")
	public String doLogin(HttpServletRequest request, Model model) throws Exception {
		if(memberDao.verifyMember(EncodingTransformer.transformEncoding(request.getParameter("userName")), 
				EncodingTransformer.transformEncoding(request.getParameter("password")))) {
			logger.debug("Logging successfully...");
			loginSuccessfully = true;
		}
		else {
			logger.debug("Logging failed...");
			return "content/login";
		}
		return "content/home";
	}
	
	@RequestMapping(value = "/telecom/home")
	public String idslTelecomMainPage() throws Exception {
		loginCheck(loginSuccessfully);
		return "content/home";
	}
	
	@RequestMapping(value = "/logout")
	public String logOut() {
		loginSuccessfully = false;
		return "content/login";
	}
	
   /* Third G services */	
	
	@RequestMapping(value = "/telecom/telecomService/thirdg/management")
	public String thirdgManagement() throws Exception {
		loginCheck(loginSuccessfully);
		return "telecom/thirdgService_mainPage";
	}
	
	@RequestMapping(value = "/telecom/telecomService/thirdg/provision")
	public String provisionPageForThirdG() throws Exception {
		loginCheck(loginSuccessfully);
		return "telecom/thirdgService_provision";
	}

	@RequestMapping(value = "/telecom/telecomService/thirdg/doProvision")
	public String provisionForThirdG(HttpServletRequest request, Model model) throws Exception {
		loginCheck(loginSuccessfully);
		logger.debug("Provision business logic executing...");
		try {
			Customer customer = CustomerDataTransformer.createCustomerFromRequest(request);
			CustomerDataTransformer.printOutCustomerProvisionData(customer);
			boolean provResult = thirdgService.provisionForCustomer(customer);
			String resultMsg = null;
			if(provResult) {
				resultMsg = "Provision Successfully!!";
			} else {
				resultMsg = "Provision Failed, please checkout the log file...";
			}
			model.addAttribute("provResult", resultMsg);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "telecom/thirdgService_provision";
	}
	
	@RequestMapping(value = "/telecom/telecomService/thirdg/suspend")
	public String suspendPageForThirdG() throws Exception {
		loginCheck(loginSuccessfully);
		return "telecom/thirdgService_suspend";
	}
	
	@RequestMapping(value = "/telecom/telecomService/thirdg/doSuspend")
	public String suspendCustomerForThirdG(HttpServletRequest request, Model model) throws Exception {
		loginCheck(loginSuccessfully);
		logger.debug("Suspend business logic executing...");
		try {
			String resultMsg = null;
			boolean suspendResult = thirdgService.suspendCustomer(EncodingTransformer.transformEncoding(request.getParameter("mdn")));
			if(suspendResult) {
				resultMsg = "Suspend Successfully!!";
			} else {
				resultMsg = "Suspend Failed, please checkout the log file...";
			}
			model.addAttribute("suspendResult", resultMsg);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "telecom/thirdgService_suspend";
	}
	
	@RequestMapping(value = "/telecom/telecomService/thirdg/reactivate")
	public String reactivatePageForThirdG() throws Exception {
		loginCheck(loginSuccessfully);
		return "telecom/thirdgService_reactivate";
	}
	
	@RequestMapping(value = "/telecom/telecomService/thirdg/doReactivate")
	public String reactivateCustomerForThirdG(HttpServletRequest request, Model model) throws Exception {
		loginCheck(loginSuccessfully);
		logger.debug("Reactivate business logic executing...");
		try {
			String resultMsg = null;
			boolean reactivateResult = thirdgService.reactivateCustomer(EncodingTransformer.transformEncoding(request.getParameter("mdn")));
			if(reactivateResult) {
				resultMsg = "Reactivate Successfully!!";
			} else {
				resultMsg = "Reactivate Failed, please checkout the log file...";
			}
			model.addAttribute("reactivateResult", resultMsg);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "telecom/thirdgService_reactivate";
	}
	
	@RequestMapping(value = "/telecom/telecomService/thirdg/queryForAllCustomer")
	public String showCustomerListForThirdG() throws Exception {
		loginCheck(loginSuccessfully);
		return "telecom/thirdgService_queryForAllCustomer";
	}
	
	@RequestMapping(value = "/telecom/telecomService/thirdg/doQueryForAllCustomer")
	public ModelAndView getCustomerListForThirdG() throws Exception {
		loginCheck(loginSuccessfully);
		List<CustomerRecordForList> customerList = thirdgService.queryAllCustomer();
		return new ModelAndView("telecom/thirdgService_queryForAllCustomer", "customerList", customerList);
	}
	
	@RequestMapping(value = "/telecom/telecomService/thirdg/queryForSpecificCustomer")
	public String  showSpecificCustomerForThirdG() throws Exception {
		loginCheck(loginSuccessfully);
		return "telecom/thirdgService_queryForSpecificCustomer";
	}
	
	@RequestMapping(value = "/telecom/telecomService/thirdg/doQueryForSpecificCustomer")
	public ModelAndView getSpecificCustomerForThirdG(HttpServletRequest request, Model model) throws Exception {
		loginCheck(loginSuccessfully);
		Customer customer = null;
		try {
			customer = thirdgService.queryCustomerWithMdn(EncodingTransformer.transformEncoding(request.getParameter("mdn")));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ModelAndView("telecom/thirdgService_queryForSpecificCustomer", "customer", customer);
	}
	
	/* Fourth G services */
	
	@RequestMapping(value = "/telecom/telecomService/fourthg/management")
	public String fourthgManagement() throws Exception {
		loginCheck(loginSuccessfully);
		return "telecom/fourthgService_mainPage";
	}
	
	@RequestMapping(value = "/telecom/telecomService/fourthg/provision")
	public String provisionPageForFourthG() throws Exception {
		loginCheck(loginSuccessfully);
		return "telecom/fourthgService_provision";
	}

	@RequestMapping(value = "/telecom/telecomService/fourthg/doProvision")
	public String provisionForFourthG(HttpServletRequest request, Model model) throws Exception {
		loginCheck(loginSuccessfully);
		logger.debug("Provision business logic executing...");
		try {
			Customer customer = CustomerDataTransformer.createCustomerFromRequest(request);
			CustomerDataTransformer.printOutCustomerProvisionData(customer);
			boolean provResult = fourthgService.provisionForCustomer(customer);
			String resultMsg = null;
			if(provResult) {
				resultMsg = "Provision Successfully!!";
			} else {
				resultMsg = "Provision Failed, please checkout the log file...";
			}
			model.addAttribute("provResult", resultMsg);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "telecom/fourthgService_provision";
	}
	
	@RequestMapping(value = "/telecom/telecomService/fourthg/suspend")
	public String suspendPageForFourthG() throws Exception {
		loginCheck(loginSuccessfully);
		return "telecom/fourthgService_suspend";
	}
	
	@RequestMapping(value = "/telecom/telecomService/fourthg/doSuspend")
	public String suspendCustomerForFourthG(HttpServletRequest request, Model model) throws Exception {
		loginCheck(loginSuccessfully);
		logger.debug("Suspend business logic executing...");
		try {
			String resultMsg = null;
			boolean suspendResult = fourthgService.suspendCustomer(EncodingTransformer.transformEncoding(request.getParameter("mdn")));
			if(suspendResult) {
				resultMsg = "Suspend Successfully!!";
			} else {
				resultMsg = "Suspend Failed, please checkout the log file...";
			}
			model.addAttribute("suspendResult", resultMsg);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "telecom/fourthgService_suspend";
	}
	
	@RequestMapping(value = "/telecom/telecomService/fourthg/reactivate")
	public String reactivatePageForFourthG() throws Exception {
		loginCheck(loginSuccessfully);
		return "telecom/fourthgService_reactivate";
	}
	
	@RequestMapping(value = "/telecom/telecomService/fourthg/doReactivate")
	public String reactivateCustomerForFourthG(HttpServletRequest request, Model model) throws Exception {
		loginCheck(loginSuccessfully);
		logger.debug("Reactivate business logic executing...");
		try {
			String resultMsg = null;
			boolean reactivateResult = fourthgService.reactivateCustomer(EncodingTransformer.transformEncoding(request.getParameter("mdn")));
			if(reactivateResult) {
				resultMsg = "Reactivate Successfully!!";
			} else {
				resultMsg = "Reactivate Failed, please checkout the log file...";
			}
			model.addAttribute("reactivateResult", resultMsg);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "telecom/fourthgService_reactivate";
	}
	
	@RequestMapping(value = "/telecom/telecomService/fourthg/queryForAllCustomer")
	public String showCustomerListForFourthG() throws Exception {
		loginCheck(loginSuccessfully);
		return "telecom/fourthgService_queryForAllCustomer";
	}
	
	@RequestMapping(value = "/telecom/telecomService/fourthg/doQueryForAllCustomer")
	public ModelAndView getCustomerListForFourthG() throws Exception {
		loginCheck(loginSuccessfully);
		List<CustomerRecordForList> customerList = fourthgService.queryAllCustomer();
		return new ModelAndView("telecom/fourthgService_queryForAllCustomer", "customerList", customerList);
	}
	
	@RequestMapping(value = "/telecom/telecomService/fourthg/queryForSpecificCustomer")
	public String  showSpecificCustomerForFourthG() throws Exception {
		loginCheck(loginSuccessfully);
		return "telecom/fourthgService_queryForSpecificCustomer";
	}
	
	@RequestMapping(value = "/telecom/telecomService/fourthg/doQueryForSpecificCustomer")
	public ModelAndView getSpecificCustomerForFourthG(HttpServletRequest request, Model model) throws Exception {
		loginCheck(loginSuccessfully);
		Customer customer = null;
		try {
			customer = fourthgService.queryCustomerWithMdn(EncodingTransformer.transformEncoding(request.getParameter("mdn")));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ModelAndView("telecom/fourthgService_queryForSpecificCustomer", "customer", customer);
	}
	
	/* CRBT services */
	
	@RequestMapping(value = "/telecom/telecomService/value_added/crbt/management")
	public ModelAndView crbtManagement() throws Exception {
		loginCheck(loginSuccessfully);
		List<Music> musicList = null;
		try {
			musicList = crbtService.listAllMusic();
			logger.debug("Obtain music list successfully...");
			logger.debug("List size: " + musicList.size());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ModelAndView("telecom/crbtService", "songList", musicList);
	}
	
	@RequestMapping(value = "/telecom/telecomService/value_added/crbt/setCRBTForSpecificCustomer")
	public ModelAndView setCRBTForSpecificCustomer(HttpServletRequest request, Model model) throws Exception{
		loginCheck(loginSuccessfully);
		String result = null;
		try {
			boolean settingResult = crbtService.setCRBTForSpecificMdn(EncodingTransformer.transformEncoding(request.getParameter("mdn")), 
					Integer.valueOf(EncodingTransformer.transformEncoding(request.getParameter("songNumber"))));
			if(settingResult) 
				result = "Set CRBT successfully!!";
			else 
				result = "Set CRBT failed...";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ModelAndView("telecom/crbtService", "setCRBTResult", result);
	}
	
	@RequestMapping(value = "/telecom/telecomService/value_added/crbt/showCRBTForSpecificCustomer")
	public ModelAndView showCRBTForSpecificCustomer(HttpServletRequest request, Model model) throws Exception {
		loginCheck(loginSuccessfully);
		Music music = null;
		try {
			music = crbtService.showCRBTInfoForSpecificMdn(EncodingTransformer.transformEncoding(request.getParameter("mdnForQuery")));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ModelAndView("telecom/crbtService", "queryResult", music);
	}
	
	/* Third g backup services */
	
	@RequestMapping(value = "/telecom/telecomService/thirdgBackup/management")
	public String thirdgBackupManagement() throws Exception {
		loginCheck(loginSuccessfully);
		return "telecom/thirdgBackupService_mainPage";
	}
	
	@RequestMapping(value = "/telecom/telecomService/thirdgBackup/provision")
	public String provisionPageForthirdgBackup() throws Exception {
		loginCheck(loginSuccessfully);
		return "telecom/thirdgBackupService_provision";
	}

	@RequestMapping(value = "/telecom/telecomService/thirdgBackup/doProvision")
	public String provisionForthirdgBackup(HttpServletRequest request, Model model) throws Exception {
		loginCheck(loginSuccessfully);
		logger.debug("Provision business logic executing...");
		try {
			Customer customer = CustomerDataTransformer.createCustomerFromRequest(request);
			CustomerDataTransformer.printOutCustomerProvisionData(customer);
			boolean provResult = thirdgServiceBackup.provisionForCustomer(customer);
			String resultMsg = null;
			if(provResult) {
				resultMsg = "Provision Successfully!!";
			} else {
				resultMsg = "Provision Failed, please checkout the log file...";
			}
			model.addAttribute("provResult", resultMsg);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "telecom/thirdgBackupService_provision";
	}
	
	@RequestMapping(value = "/telecom/telecomService/thirdgBackup/suspend")
	public String suspendPageForthirdgBackup() throws Exception {
		loginCheck(loginSuccessfully);
		return "telecom/thirdgBackupService_suspend";
	}
	
	@RequestMapping(value = "/telecom/telecomService/thirdgBackup/doSuspend")
	public String suspendCustomerForthirdgBackup(HttpServletRequest request, Model model) throws Exception {
		loginCheck(loginSuccessfully);
		logger.debug("Suspend business logic executing...");
		try {
			String resultMsg = null;
			boolean suspendResult = thirdgServiceBackup.suspendCustomer(EncodingTransformer.transformEncoding(request.getParameter("mdn")));
			if(suspendResult) {
				resultMsg = "Suspend Successfully!!";
			} else {
				resultMsg = "Suspend Failed, please checkout the log file...";
			}
			model.addAttribute("suspendResult", resultMsg);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "telecom/thirdgBackupService_suspend";
	}
	
	@RequestMapping(value = "/telecom/telecomService/thirdgBackup/reactivate")
	public String reactivatePageForthirdgBackup() throws Exception {
		loginCheck(loginSuccessfully);
		return "telecom/thirdgBackupService_reactivate";
	}
	
	@RequestMapping(value = "/telecom/telecomService/thirdgBackup/doReactivate")
	public String reactivateCustomerForthirdgBackup(HttpServletRequest request, Model model) throws Exception {
		loginCheck(loginSuccessfully);
		logger.debug("Reactivate business logic executing...");
		try {
			String resultMsg = null;
			boolean reactivateResult = thirdgServiceBackup.reactivateCustomer(EncodingTransformer.transformEncoding(request.getParameter("mdn")));
			if(reactivateResult) {
				resultMsg = "Reactivate Successfully!!";
			} else {
				resultMsg = "Reactivate Failed, please checkout the log file...";
			}
			model.addAttribute("reactivateResult", resultMsg);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "telecom/thirdgBackupService_reactivate";
	}
	
	@RequestMapping(value = "/telecom/telecomService/thirdgBackup/queryForAllCustomer")
	public String showCustomerListForthirdgBackup() throws Exception {
		loginCheck(loginSuccessfully);
		return "telecom/thirdgBackupService_queryForAllCustomer";
	}
	
	@RequestMapping(value = "/telecom/telecomService/thirdgBackup/doQueryForAllCustomer")
	public ModelAndView getCustomerListForthirdgBackup() throws Exception {
		loginCheck(loginSuccessfully);
		List<CustomerRecordForList> customerList = thirdgServiceBackup.queryAllCustomer();
		return new ModelAndView("telecom/thirdgBackupService_queryForAllCustomer", "customerList", customerList);
	}
	
	@RequestMapping(value = "/telecom/telecomService/thirdgBackup/queryForSpecificCustomer")
	public String  showSpecificCustomerForthirdgBackup() throws Exception {
		loginCheck(loginSuccessfully);
		return "telecom/thirdgBackupService_queryForSpecificCustomer";
	}
	
	@RequestMapping(value = "/telecom/telecomService/thirdgBackup/doQueryForSpecificCustomer")
	public ModelAndView getSpecificCustomerForthirdgBackup(HttpServletRequest request, Model model) throws Exception {
		loginCheck(loginSuccessfully);
		Customer customer = null;
		try {
			customer = thirdgServiceBackup.queryCustomerWithMdn(EncodingTransformer.transformEncoding(request.getParameter("mdn")));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ModelAndView("telecom/thirdgBackupService_queryForSpecificCustomer", "customer", customer);
	}
	
	private void loginCheck(boolean loginSuccessfully) throws Exception {
		if(!loginSuccessfully)
			throw new Exception("Login failed.");
	}

}