package ntust.idsl.telecom.web.service;

import java.util.List;

import ntust.idsl.telecom.web.stub.crbt.MusicServerStub.Music;

/**
 * @author Carl Adler(C.A.)
 * */
public interface CRBTService {
	
	public boolean setCRBTForSpecificMdn(String mdn, int songNumber) throws Exception;
	public List<Music> listAllMusic() throws Exception;
	public Music showCRBTInfoForSpecificMdn(String mdn) throws Exception;
	
}
