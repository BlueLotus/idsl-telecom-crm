package ntust.idsl.telecom.web.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import ntust.idsl.telecom.web.dao.ThirdgServiceDao;
import ntust.idsl.telecom.web.dto.Customer;
import ntust.idsl.telecom.web.dto.CustomerRecordForList;
import ntust.idsl.telecom.web.service.ThirdgService;
import ntust.idsl.telecom.web.stub.thirdg.ThirdgServiceImplStub;
import ntust.idsl.telecom.web.stub.thirdg.ThirdgServiceImplStub.ProvisionForCustomer;
import ntust.idsl.telecom.web.stub.thirdg.ThirdgServiceImplStub.ReactivateCustomer;
import ntust.idsl.telecom.web.stub.thirdg.ThirdgServiceImplStub.SuspendCustomer;
import ntust.idsl.telecom.web.util.CustomerDataTransformer;

/**
 * @author Carl Adler(C.A.)
 * */
public class ThirdgServiceImpl implements ThirdgService{
	
	private static final Logger logger = Logger.getLogger(ThirdgServiceImpl.class);

	@Autowired
	private ThirdgServiceDao thirdgServiceDao;
	
	@Autowired
	private ThirdgServiceImplStub thirdgServiceStub;
	
	@Override
	public boolean provisionForCustomer(Customer customer) throws Exception {
		boolean provResult =false;
		ProvisionForCustomer provisionForCustomer = new ProvisionForCustomer();
		provisionForCustomer.setRecord(CustomerDataTransformer.transformCustomerDataForThirdGProvision(customer));
		boolean serviceProvResult = thirdgServiceStub.provisionForCustomer(provisionForCustomer).get_return();
		if(serviceProvResult) {
			int nextCustomerCount = thirdgServiceDao.getNextCustomerCount();
			if(thirdgServiceDao.saveCustomer(customer, nextCustomerCount) && thirdgServiceDao.saveServiceForCustomer(customer, nextCustomerCount)) {
				provResult = true;
				logger.debug("Provision Successfully...");
			} else {
				logger.debug("Local persistent access failed...");
			}
		} else {
			logger.debug("Provision Failed...");
		}
		return provResult;
	}

	@Override
	public boolean suspendCustomer(String mdn) throws Exception {
		boolean suspendResult = false;
		SuspendCustomer suspendCustomer = new SuspendCustomer();
		suspendCustomer.setMdn(mdn);
		boolean serviceSuspendResult = thirdgServiceStub.suspendCustomer(suspendCustomer).get_return();
		if(serviceSuspendResult) {
			if(thirdgServiceDao.suspendCustomer(mdn)){
				suspendResult =true;
				logger.debug("Suspend Successfully...");
			} else {
				logger.debug("Local persistent access failed...");
			}
		} else {
			logger.debug("Suspend Failed...");
		}
		return suspendResult;
	}

	@Override
	public boolean reactivateCustomer(String mdn) throws Exception {
		boolean reactivateResult = false;
		ReactivateCustomer reactivateCustomer = new ReactivateCustomer();
		reactivateCustomer.setMdn(mdn);
		boolean serviceReactivateResult = thirdgServiceStub.reactivateCustomer(reactivateCustomer).get_return();
		if(serviceReactivateResult) {
			if(thirdgServiceDao.reactivateCustomer(mdn)) {
				reactivateResult = true;
				logger.debug("Reactivate Successfully...");
			} else {
				logger.debug("Local persistent access failed...");
			}
		} else {
			logger.debug("Reactivate Failed...");
		}
		return reactivateResult;
	}

	@Override
	public Customer queryCustomerWithMdn(String mdn) {
		return thirdgServiceDao.querySpecificCustomerWithMdn(mdn);
	}

	@Override
	public List<CustomerRecordForList> queryAllCustomer() {
		return thirdgServiceDao.queryForAllCustomer();
	}

}
