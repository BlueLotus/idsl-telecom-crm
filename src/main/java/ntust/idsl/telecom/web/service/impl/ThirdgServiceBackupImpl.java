package ntust.idsl.telecom.web.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import ntust.idsl.telecom.web.dao.ThirdgServiceBackupDao;
import ntust.idsl.telecom.web.dto.Customer;
import ntust.idsl.telecom.web.dto.CustomerRecordForList;
import ntust.idsl.telecom.web.service.ThirdgServiceBackup;
import ntust.idsl.telecom.web.stub.thirdg.ThirdgBackup;
import ntust.idsl.telecom.web.stub.thirdg.ThirdgBackup.ProvisionForCustomer;
import ntust.idsl.telecom.web.stub.thirdg.ThirdgBackup.ReactiveCustomer;
import ntust.idsl.telecom.web.stub.thirdg.ThirdgBackup.SuspendCustomer;
import ntust.idsl.telecom.web.util.CustomerDataTransformer;

/**
 * @author Carl Adler(C.A.)
 * */
public class ThirdgServiceBackupImpl implements ThirdgServiceBackup {
	
	private static final Logger logger = Logger.getLogger(ThirdgServiceBackupImpl.class);
	
	@Autowired
	private ThirdgServiceBackupDao thirdgServiceBackupDao;
	
	@Autowired
	private ThirdgBackup thirdgServiceBackupStub;
	
	@Override
	public boolean provisionForCustomer(Customer customer) throws Exception {
		boolean provResult =false;
		ProvisionForCustomer provisionForCustomer = new ProvisionForCustomer();
		provisionForCustomer.setRecord(CustomerDataTransformer.transformCustomerDataForThirdGBackupProvision(customer));
		boolean serviceProvResult = thirdgServiceBackupStub.provisionForCustomer(provisionForCustomer).get_return();
		if(serviceProvResult) {
			int nextCustomerCount = thirdgServiceBackupDao.getNextCustomerCount();
			if(thirdgServiceBackupDao.saveCustomer(customer, nextCustomerCount) && thirdgServiceBackupDao.saveServiceForCustomer(customer, nextCustomerCount)) {
				provResult = true;
				logger.debug("Provision Successfully...");
			} else {
				logger.debug("Local persistent access failed...");
			}
		} else {
			logger.debug("Provision Failed...");
		}
		return provResult;
	}

	@Override
	public boolean suspendCustomer(String mdn) throws Exception {
		boolean suspendResult = false;
		SuspendCustomer suspendCustomer = new SuspendCustomer();
		suspendCustomer.setMdn(mdn);
		boolean serviceSuspendResult = thirdgServiceBackupStub.suspendCustomer(suspendCustomer).get_return();
		if(serviceSuspendResult) {
			if(thirdgServiceBackupDao.suspendCustomer(mdn)){
				suspendResult =true;
				logger.debug("Suspend Successfully...");
			} else {
				logger.debug("Local persistent access failed...");
			}
		} else {
			logger.debug("Suspend Failed...");
		}
		return suspendResult;
	}

	@Override
	public boolean reactivateCustomer(String mdn) throws Exception {
		boolean reactivateResult = false;
		ReactiveCustomer reactivateCustomer = new ReactiveCustomer();
		reactivateCustomer.setMdn(mdn);
		boolean serviceReactivateResult = thirdgServiceBackupStub.reactiveCustomer(reactivateCustomer).get_return();
		if(serviceReactivateResult) {
			if(thirdgServiceBackupDao.reactivateCustomer(mdn)) {
				reactivateResult = true;
				logger.debug("Reactivate Successfully...");
			} else {
				logger.debug("Local persistent access failed...");
			}
		} else {
			logger.debug("Reactivate Failed...");
		}
		return reactivateResult;
	}

	@Override
	public Customer queryCustomerWithMdn(String mdn) {
		return thirdgServiceBackupDao.querySpecificCustomerWithMdn(mdn);
	}

	@Override
	public List<CustomerRecordForList> queryAllCustomer() {
		return thirdgServiceBackupDao.queryForAllCustomer();
	}
}
