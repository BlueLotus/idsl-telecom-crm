package ntust.idsl.telecom.web.service;

import java.util.List;

import ntust.idsl.telecom.web.dto.Customer;
import ntust.idsl.telecom.web.dto.CustomerRecordForList;

/**
 * @author Carl Adler(C.A.)
 * */
public interface FourthgService {

	public boolean provisionForCustomer(Customer customer) throws Exception;
	public boolean suspendCustomer(String mdn) throws Exception;
	public boolean reactivateCustomer(String mdn) throws Exception;
	public Customer queryCustomerWithMdn(String mdn);
	public List<CustomerRecordForList> queryAllCustomer();
	
}
