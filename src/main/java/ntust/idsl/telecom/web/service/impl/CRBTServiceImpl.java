package ntust.idsl.telecom.web.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ntust.idsl.telecom.web.service.CRBTService;
import ntust.idsl.telecom.web.stub.crbt.MusicServerStub;
import ntust.idsl.telecom.web.stub.crbt.MusicServerStub.ListAllMusic;
import ntust.idsl.telecom.web.stub.crbt.MusicServerStub.Music;
import ntust.idsl.telecom.web.stub.crbt.MusicServerStub.SetCRBTWithMusic;
import ntust.idsl.telecom.web.stub.crbt.MusicServerStub.ShowCurrentCRBT;

/**
 * @author Carl Adler(C.A.)
 * */
public class CRBTServiceImpl implements CRBTService {
	
	@Autowired
	private MusicServerStub crbtServiceStub;

	@Override
	public boolean setCRBTForSpecificMdn(String mdn, int songNumber) throws Exception {
		SetCRBTWithMusic setCRBTWithMusic = new SetCRBTWithMusic();
		setCRBTWithMusic.setMdn(mdn);
		setCRBTWithMusic.setSongNumber(songNumber);
		return crbtServiceStub.setCRBTWithMusic(setCRBTWithMusic).get_return();
	}

	@Override
	public List<Music> listAllMusic() throws Exception {
		ListAllMusic listAllMusic = new ListAllMusic();
		return transformMusicArrayToList(crbtServiceStub.listAllMusic(listAllMusic).get_return());
	}

	@Override
	public Music showCRBTInfoForSpecificMdn(String mdn) throws Exception {
		ShowCurrentCRBT showCurrentCRBT = new ShowCurrentCRBT();
		showCurrentCRBT.setMdn(mdn);
		return crbtServiceStub.showCurrentCRBT(showCurrentCRBT).get_return();
	}
	
	private List<Music> transformMusicArrayToList(Music[] musicArray) {
		List<Music> list = new ArrayList<Music>();
		int size = musicArray.length;
		
		for(int i = 0; i < size; i++) {
			Music music = new Music();
			music.setSongNumber(musicArray[i].getSongNumber());
			System.out.println("songNumber: " + musicArray[i].getSongNumber());
			music.setName(musicArray[i].getName());
			System.out.println("name: " + musicArray[i].getName());
			music.setLanguage(musicArray[i].getLanguage());
			System.out.println("language: " + musicArray[i].getLanguage());
			list.add(music);
		}
		
		return list;
	}

}
