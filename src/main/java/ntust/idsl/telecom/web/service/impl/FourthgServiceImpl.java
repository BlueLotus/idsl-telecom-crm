package ntust.idsl.telecom.web.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import ntust.idsl.telecom.web.dao.FourthgServiceDao;
import ntust.idsl.telecom.web.dto.Customer;
import ntust.idsl.telecom.web.dto.CustomerRecordForList;
import ntust.idsl.telecom.web.service.FourthgService;
import ntust.idsl.telecom.web.stub.fourthg.FourthgServiceImplStub;
import ntust.idsl.telecom.web.stub.fourthg.FourthgServiceImplStub.ProvisionForCustomer;
import ntust.idsl.telecom.web.stub.fourthg.FourthgServiceImplStub.ReactivateCustomer;
import ntust.idsl.telecom.web.stub.fourthg.FourthgServiceImplStub.SuspendCustomer;
import ntust.idsl.telecom.web.util.CustomerDataTransformer;

/**
 * @author Carl Adler(C.A.)
 * */
public class FourthgServiceImpl implements FourthgService{
	
	private static final Logger logger = Logger.getLogger(FourthgServiceImpl.class);

	@Autowired
	private FourthgServiceDao fourthgServiceDao;
	
	@Autowired
	private FourthgServiceImplStub fourthgServiceStub;
	
	@Override
	public boolean provisionForCustomer(Customer customer) throws Exception {
		boolean provResult =false;
		ProvisionForCustomer provisionForCustomer = new ProvisionForCustomer();
		provisionForCustomer.setRecord(CustomerDataTransformer.transformCustomerDataForFourthGProvision(customer));
		boolean serviceProvResult = fourthgServiceStub.provisionForCustomer(provisionForCustomer).get_return();
		if(serviceProvResult) {
			int nextCustomerCount = fourthgServiceDao.getNextCustomerCount();
			if(fourthgServiceDao.saveCustomer(customer, nextCustomerCount) && fourthgServiceDao.saveServiceForCustomer(customer, nextCustomerCount)) {
				provResult = true;
				logger.debug("Provision Successfully...");
			} else {
				logger.debug("Local persistent access failed...");
			}
		} else {
			logger.debug("Provision Failed...");
		}
		return provResult;
	}

	@Override
	public boolean suspendCustomer(String mdn) throws Exception {
		boolean suspendResult = false;
		SuspendCustomer suspendCustomer = new SuspendCustomer();
		suspendCustomer.setMdn(mdn);
		boolean serviceSuspendResult = fourthgServiceStub.suspendCustomer(suspendCustomer).get_return();
		if(serviceSuspendResult) {
			if(fourthgServiceDao.suspendCustomer(mdn)){
				suspendResult =true;
				logger.debug("Suspend Successfully...");
			} else {
				logger.debug("Local persistent access failed...");
			}
		} else {
			logger.debug("Suspend Failed...");
		}
		return suspendResult;
	}

	@Override
	public boolean reactivateCustomer(String mdn) throws Exception {
		boolean reactivateResult = false;
		ReactivateCustomer reactivateCustomer = new ReactivateCustomer();
		reactivateCustomer.setMdn(mdn);
		boolean serviceReactivateResult = fourthgServiceStub.reactivateCustomer(reactivateCustomer).get_return();
		if(serviceReactivateResult) {
			if(fourthgServiceDao.reactivateCustomer(mdn)) {
				reactivateResult = true;
				logger.debug("Reactivate Successfully...");
			} else {
				logger.debug("Local persistent access failed...");
			}
		} else {
			logger.debug("Reactivate Failed...");
		}
		return reactivateResult;
	}

	@Override
	public Customer queryCustomerWithMdn(String mdn) {
		return fourthgServiceDao.querySpecificCustomerWithMdn(mdn);
	}

	@Override
	public List<CustomerRecordForList> queryAllCustomer() {
		return fourthgServiceDao.queryForAllCustomer();
	}

}
