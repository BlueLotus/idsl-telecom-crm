package ntust.idsl.telecom.web.dao.impl;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

/**
 * @author Carl Adler(C.A.)
 * */
public class MemberDao extends JdbcDaoSupport {
	private final String queryForMember = "select password from member where username = ?;";
	
	public boolean verifyMember(String userName, String password) {
		return password.equalsIgnoreCase(getJdbcTemplate().queryForObject(queryForMember, String.class, new Object[] {userName}));
	}
}
