package ntust.idsl.telecom.web.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import ntust.idsl.telecom.web.dao.FourthgServiceDao;
import ntust.idsl.telecom.web.dto.Customer;
import ntust.idsl.telecom.web.dto.CustomerRecordForList;
import ntust.idsl.telecom.web.dto.rowmapper.CustomerRowMapperForFourthg;
import ntust.idsl.telecom.web.util.DateHandler;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

/**
 * @author Carl Adler(C.A.)
 * */
public class FourthgServiceDaoImpl extends JdbcDaoSupport implements FourthgServiceDao {

	private final String queryForSaveCustomer ="INSERT INTO customer (customerID, customerName, age, gender, birthday, address, contactPhone, fourthgMdn) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?);";
	private final String queryForCustomerCount = "select count(*) from customer;";
	private final String queryForSaveFourthgService = "INSERT INTO fourthg_service (customerID, serviceType, provStatus, voiceRateID, mobileRateID, effectedDate, terminationDate, contractType) VALUES (?, '4G', 'Activate', ?, ?, ?, ?, ?);";
	private final String queryForAllCustomer = "select a.customerID, a.customerName, a.fourthgMdn, c.fee voice_fee, d.fee mobile_fee, b.contractType, b.provStatus, b.effectedDate, b.terminationDate "
			+ "from customer a, fourthg_service b, fourthg_voice_rate c, fourthg_mobile_net_rate d where (a.customerID = b.customerID) AND (b.voiceRateID = c.voiceRateID) AND (b.mobileRateID = d.mobileRateID);";
	private final String queryForModifyCustomerProvStatus = "UPDATE fourthg_service SET provStatus = ? WHERE customerID = (select customerID from customer where fourthgMdn=?);";
	private final String queryForSpecificCustomerWithMdn = "select a.customerID, a.customerName, a.age, a.gender, a.birthday, a.address, a.contactPhone, "
			+ "a.fourthgMdn, b.provStatus, b.voiceRateID, c.fee voice_fee, b.mobileRateID, d.fee mobile_fee, b.effectedDate, b.terminationDate, b.contractType "
			+ "from customer a, fourthg_service b, fourthg_voice_rate c, fourthg_mobile_net_rate d "
			+ "where (a.customerID = b.customerID) AND (b.voiceRateID = c.voiceRateID) AND (b.mobileRateID = d.mobileRateID) AND (a.fourthgMdn = ?);";

	
	@Override
	public boolean saveCustomer(Customer customer, int nextCustomerCount) {
		boolean result = false;
		int updateResult = getJdbcTemplate().update(queryForSaveCustomer, new Object[] {
				nextCustomerCount, customer.getCustomerName(), customer.getAge(), customer.getGender(), 
				DateHandler.getDateWithString(customer.getBirthday()), 
				customer.getAddress(), customer.getContactPhone(), customer.getMdnForFourthg()});
		if(updateResult > 0)
			result = true;
		return result;
	}

	@Override
	public boolean saveServiceForCustomer(Customer customer, int nextCustomerCount) {
		boolean result = false;
		int updateResult = getJdbcTemplate().update(queryForSaveFourthgService, new Object[] {
				nextCustomerCount, customer.getVoiceRateIdForFourthg(), customer.getMobileRateIdForFourthg(), 
				DateHandler.getDateWithString(customer.getEffectedDateForFourthg()),
				DateHandler.getDateWithString(customer.getTerminationDateForFourthg()),
				customer.getContractTypeForFourthg()});
		if(updateResult > 0)
			result = true;
		return result;
	}

	@Override
	public boolean suspendCustomer(String mdn) {
		boolean result = false;
		int updateResult = getJdbcTemplate().update(queryForModifyCustomerProvStatus, new Object[] {"Suspend", mdn});
		if(updateResult > 0)
			result = true;
		return result;
	}

	@Override
	public boolean reactivateCustomer(String mdn) {
		boolean result = false;
		int updateResult = getJdbcTemplate().update(queryForModifyCustomerProvStatus, new Object[] {"Activate", mdn});
		if(updateResult > 0)
			result = true;
		return result;
	}

	@Override
	public Customer querySpecificCustomerWithMdn(String mdn) {
		return getJdbcTemplate().queryForObject(queryForSpecificCustomerWithMdn, new Object[] {mdn}, new CustomerRowMapperForFourthg());
	}

	@Override
	public List<CustomerRecordForList> queryForAllCustomer() {
		List<CustomerRecordForList> customerList = new ArrayList<CustomerRecordForList>();
		List<Map<String, Object>> rows = getJdbcTemplate().queryForList(queryForAllCustomer);
		for (Map<String, Object> map : rows) {
			CustomerRecordForList cRecordForList = new CustomerRecordForList();
			cRecordForList.setCustomerId((Integer)map.get("customerID"));
			cRecordForList.setCustomerName((String)map.get("customerName"));
			cRecordForList.setMdn((String)map.get("fourthgMdn"));
			cRecordForList.setVoiceFee((Integer)map.get("voice_fee"));
			cRecordForList.setMobileFee((Integer)map.get("mobile_fee"));
			cRecordForList.setContractType((String)map.get("contractType"));
			cRecordForList.setProvStatus((String)map.get("provStatus"));
			cRecordForList.setEffectedDate(DateHandler.getStringDateFromDateObj((Date)map.get("effectedDate")));
			cRecordForList.setTerminationDate(DateHandler.getStringDateFromDateObj((Date)map.get("terminationDate")));
			customerList.add(cRecordForList);
		}
		return customerList;
	}
	
	@Override
	public int getNextCustomerCount() {
		int customerCount = getJdbcTemplate().queryForObject(queryForCustomerCount, Integer.class);
		return (customerCount + 1);
	}

}
