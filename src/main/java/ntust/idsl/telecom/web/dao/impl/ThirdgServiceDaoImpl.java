package ntust.idsl.telecom.web.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

import ntust.idsl.telecom.web.dao.ThirdgServiceDao;
import ntust.idsl.telecom.web.dto.Customer;
import ntust.idsl.telecom.web.dto.CustomerRecordForList;
import ntust.idsl.telecom.web.dto.rowmapper.CustomerRowMapperForThirdg;
import ntust.idsl.telecom.web.util.DateHandler;

/**
 * @author Carl Adler(C.A.)
 * */
public class ThirdgServiceDaoImpl extends JdbcDaoSupport implements ThirdgServiceDao {
	
	private final String queryForSaveCustomer ="INSERT INTO customer (customerID, customerName, age, gender, birthday, address, contactPhone, thirdgMdn) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?);";
	private final String queryForCustomerCount = "select count(*) from customer;";
	private final String queryForSaveThirdgService = "INSERT INTO thirdg_service (customerID, serviceType, provStatus, voiceRateID, mobileRateID, effectedDate, terminationDate, contractType) VALUES (?, '3G', 'Activate', ?, ?, ?, ?, ?);";
	private final String queryForAllCustomer = "select a.customerID, a.customerName, a.thirdgMdn, c.fee voice_fee, d.fee mobile_fee, b.contractType, b.provStatus, b.effectedDate, b.terminationDate "
			+ "from customer a, thirdg_service b, thirdg_voice_rate c, thirdg_mobile_net_rate d where (a.customerID = b.customerID) AND (b.voiceRateID = c.voiceRateID) AND (b.mobileRateID = d.mobileRateID);";
	private final String queryForModifyCustomerProvStatus = "UPDATE thirdg_service SET provStatus = ? WHERE customerID = (select customerID from customer where thirdgMdn=?);";
	private final String queryForSpecificCustomerWithMdn = "select a.customerID, a.customerName, a.age, a.gender, a.birthday, a.address, a.contactPhone, "
			+ "a.thirdgMdn, b.provStatus, b.voiceRateID, c.fee voice_fee, b.mobileRateID, d.fee mobile_fee, b.effectedDate, b.terminationDate, b.contractType "
			+ "from customer a, thirdg_service b, thirdg_voice_rate c, thirdg_mobile_net_rate d "
			+ "where (a.customerID = b.customerID) AND (b.voiceRateID = c.voiceRateID) AND (b.mobileRateID = d.mobileRateID) AND (a.thirdgMdn = ?);";

	@Override
	public boolean saveCustomer(Customer customer, int nextCustomerCount) {
		boolean result = false;
		int updateResult = getJdbcTemplate().update(queryForSaveCustomer, new Object[] {
				nextCustomerCount, customer.getCustomerName(), customer.getAge(), customer.getGender(), 
				DateHandler.getDateWithString(customer.getBirthday()), 
				customer.getAddress(), customer.getContactPhone(), customer.getMdnForThirdg()});
		if(updateResult > 0)
			result = true;
		return result;
	}

	@Override
	public boolean saveServiceForCustomer(Customer customer, int nextCustomerCount) {
		boolean result = false;
		int updateResult = getJdbcTemplate().update(queryForSaveThirdgService, new Object[] {
				nextCustomerCount, customer.getVoiceRateIdForThirdg(), customer.getMobileRateIdForThirdg(), 
				DateHandler.getDateWithString(customer.getEffectedDateForThirdg()),
				DateHandler.getDateWithString(customer.getTerminationDateForThirdg()),
				customer.getContractTypeForThirdg()});
		if(updateResult > 0)
			result = true;
		return result;
	}
	
	@Override
	public boolean suspendCustomer(String mdn) {
		boolean result = false;
		int updateResult = getJdbcTemplate().update(queryForModifyCustomerProvStatus, new Object[] {"Suspend", mdn});
		if(updateResult > 0)
			result = true;
		return result;
	}
	
	@Override
	public boolean reactivateCustomer(String mdn) {
		boolean result = false;
		int updateResult = getJdbcTemplate().update(queryForModifyCustomerProvStatus, new Object[] {"Activate", mdn});
		if(updateResult > 0)
			result = true;
		return result;
	}
	
	@Override
	public Customer querySpecificCustomerWithMdn(String mdn) {
		return getJdbcTemplate().queryForObject(queryForSpecificCustomerWithMdn, new Object[] {mdn}, new CustomerRowMapperForThirdg());
	}

	@Override
	public List<CustomerRecordForList> queryForAllCustomer() {
		List<CustomerRecordForList> customerList = new ArrayList<CustomerRecordForList>();
		List<Map<String, Object>> rows = getJdbcTemplate().queryForList(queryForAllCustomer);
		for (Map<String, Object> map : rows) {
			CustomerRecordForList cRecordForList = new CustomerRecordForList();
			cRecordForList.setCustomerId((Integer)map.get("customerID"));
			cRecordForList.setCustomerName((String)map.get("customerName"));
			cRecordForList.setMdn((String)map.get("thirdgMdn"));
			cRecordForList.setVoiceFee((Integer)map.get("voice_fee"));
			cRecordForList.setMobileFee((Integer)map.get("mobile_fee"));
			cRecordForList.setContractType((String)map.get("contractType"));
			cRecordForList.setProvStatus((String)map.get("provStatus"));
			cRecordForList.setEffectedDate(DateHandler.getStringDateFromDateObj((Date)map.get("effectedDate")));
			cRecordForList.setTerminationDate(DateHandler.getStringDateFromDateObj((Date)map.get("terminationDate")));
			customerList.add(cRecordForList);
		}
		return customerList;
	}
	
	@Override
	public int getNextCustomerCount() {
		int customerCount = getJdbcTemplate().queryForObject(queryForCustomerCount, Integer.class);
		return (customerCount + 1);
	}

}
