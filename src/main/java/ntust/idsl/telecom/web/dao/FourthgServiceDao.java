package ntust.idsl.telecom.web.dao;

import java.util.List;

import ntust.idsl.telecom.web.dto.Customer;
import ntust.idsl.telecom.web.dto.CustomerRecordForList;

/**
 * @author Carl Adler(C.A.)
 * */
public interface FourthgServiceDao {
	
	public boolean saveCustomer(Customer customer, int customerCount);

	public boolean saveServiceForCustomer(Customer customer, int customerCount);
	
	public boolean suspendCustomer(String mdn);
	
	public boolean reactivateCustomer(String mdn);
	
	public Customer querySpecificCustomerWithMdn(String mdn);
	
	public List<CustomerRecordForList> queryForAllCustomer();

	public int getNextCustomerCount();
	
}
