<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title><sitemesh:write property='title'/></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
   		<meta name="author" content="Carl Adler, C.A.">
		<sitemesh:write property='head'/>
		<!-- Including CSS, JS files here... -->
		<link href='<c:url value="/css/defaultTheme.css"/>' rel="stylesheet" media="screen">
		<link href='<c:url value="/bootstrapcss/bootstrap.min.css"/>' rel="stylesheet" media="screen">
		<style type="text/css">
	      body {
	        padding-top: 20px;
	        padding-bottom: 60px;
	      }

	      /* Custom container */
	      .container {
	        margin: 0 auto;
	        max-width: 1000px;
	      }
	      .container > hr {
	        margin: 60px 0;
	      }
	
	      /* Main marketing message and sign up button */
	      .jumbotron {
	        margin: 80px 0;
	        text-align: center;
	      }
	      .jumbotron h1 {
	        font-size: 100px;
	        line-height: 1;
	      }
	      .jumbotron .lead {
	        font-size: 24px;
	        line-height: 1.25;
	      }
	      .jumbotron .btn {
	        font-size: 21px;
	        padding: 14px 24px;
	      }
	
	      /* Supporting marketing content */
	      .marketing {
	        margin: 60px 0;
	      }
	      .marketing p + h4 {
	        margin-top: 28px;
	      }
	
	
	      /* Customize the navbar links to be fill the entire space of the .navbar */
	      .navbar .navbar-inner {
	        padding: 0;
	      }
	      .navbar .nav {
	        margin: 0;
	        display: table;
	        width: 100%;
	      }
	      .navbar .nav li {
	        display: table-cell;
	        width: 1%;
	        float: none;
	      }
	      .navbar .nav li a {
	        font-weight: bold;
	        text-align: center;
	        border-left: 1px solid rgba(255,255,255,.75);
	        border-right: 1px solid rgba(0,0,0,.1);
	      }
	      .navbar .nav li:first-child a {
	        border-left: 0;
	        border-radius: 3px 0 0 3px;
	      }
	      .navbar .nav li:last-child a {
	        border-right: 0;
	        border-radius: 0 3px 3px 0;
	      }
	      .container #mainbody {
			　padding-bottom: 100px;
		  }
		  .footer #mainfooter {
			　height: 100px;
			　position: relative;
			　margin-top: -100px;
		  }
    	</style>
	</head>
	<body>
		<div class="container">
			<div class="masthead">
				<!-- Menu Content -->
				<h3 class="muted">IDSL Telecom</h3>
                <div class="navbar">
                    <div class="navbar-inner">
                        <div class="container">
                            <ul class="nav">
                                <li><a href="/idsl-telecom-crm/telecom/home">Home</a></li>
                                <li><a href="/idsl-telecom-crm/telecom/telecomService/thirdg/management">Third G Service</a></li>
                                <li><a href="/idsl-telecom-crm/telecom/telecomService/fourthg/management">Fourth G Service</a></li>
                                <li><a href="/idsl-telecom-crm/telecom/telecomService/value_added/crbt/management">CRBT Service</a></li>
                                <li><a href="/idsl-telecom-crm/telecom/telecomService/thirdgBackup/management">Third G Backup</a></li>
                                <li><a href="/idsl-telecom-crm/logout">Log out</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
			</div>
			<div class="container" id="mainbody">
				<sitemesh:write property='body'/>
			</div>
			<div class="footer" id="mainfooter">
				<br/>
				<p class="quepaso">IDSL  &copy; Dep. I.M. - NTUST - 2014</p>
	        </div>
        </div>
	</body>
</html>