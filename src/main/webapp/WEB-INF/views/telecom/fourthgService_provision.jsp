<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
	<head>
		<title>IDSL Telecom - FourthG Service Provision Page</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	</head>
	<body>
		<h2>FourthG provision for customer:</h2>
		<section>
			<legend>Voice Rate and Mobile Rate table:</legend>
			<table border="1px">
				<tr>
					<td>Voice Rate ID</td>
					<td>Fee</td>
				</tr>
				<tr>
					<td>1</td>
					<td>120</td>
				</tr>
				<tr>
					<td>2</td>
					<td>300</td>
				</tr>
				<tr>
					<td>3</td>
					<td>588</td>
				</tr>
				<tr>
					<td>4</td>
					<td>850</td>
				</tr>
				<tr>
					<td>5</td>
					<td>1100</td>
				</tr>
			</table>
			<br/><br/>
			<table border="1px">
				<tr>
					<td>Mobile Rate ID</td>
					<td>Fee</td>
				</tr>
				<tr>
					<td>1</td>
					<td>200</td>
				</tr>
				<tr>
					<td>2</td>
					<td>380</td>
				</tr>
				<tr>
					<td>3</td>
					<td>520</td>
				</tr>
				<tr>
					<td>4</td>
					<td>777</td>
				</tr>
				<tr>
					<td>5</td>
					<td>900</td>
				</tr>
				<tr>
					<td>6</td>
					<td>1180</td>
				</tr>
			</table>
		</section>
		<section>
			<form action="/idsl-telecom-crm/telecom/telecomService/fourthg/doProvision" method="post">
				<fieldset>
					<legend>Please enter the customer data:</legend>
					<p>
						<label>Customer Name</label>
						<input type="text" name="customerName" value=""/>
					</p>
					<p>
						<label>Age</label>
						<input type="text" name="age" value=""/>
					</p>
					<p>
						<label>Gender</label>
						<input type="text" name="gender" value=""/>
					</p>
					<p>
						<label>Birthday(yyyy/MM/dd)</label>
						<input type="text" name="birthday" value=""/>
					</p>
					<p>
						<label>Address</label>
						<input type="text" name="address" value=""/>
					</p>
					<p>
						<label>Contact Phone</label>
						<input type="text" name="contactPhone" value=""/>
					</p>
					
					<p>
						<label>MDN for Thirdg</label>
						<input type="text" name="fourthgMdn" value=""/>
					</p>
					<p>
						<label>Voice Rate ID for Thirdg</label>
						<input type="text" name="fourthgVoiceRateId" value=""/>
					</p>
					<p>
						<label>Mobile Rate ID for Thirdg</label>
						<input type="text" name="fourthgMobileRateId" value=""/>
					</p>
					<p>
						<label>Contract Type for Thirdg</label>
						<input type="text" name="fourthgContractType" value=""/>
					</p>
					<p>
						<label>Effected Date for Thirdg</label>
						<input type="text" name="fourthgEffectedDate" value=""/>
					</p>
					<p>
						<label>Termination Date for Thirdg</label>
						<input type="text" name="fourthgTerminationDate" value=""/>
					</p>
					<p>
						<input type="submit" value="Submit"/>
					</p>
				</fieldset>
			</form>
			<c:out value="${provResult}"></c:out><br/>
		</section>
	</body>
</html>