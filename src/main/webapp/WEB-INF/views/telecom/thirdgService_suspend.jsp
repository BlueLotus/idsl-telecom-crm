<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
	<head>
		<title>IDSL Telecom - ThirdG Service Suspend Page</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	</head>
	<body>
		<h2>ThirdG suspend for customer:</h2>
		<section>
			<form action="/idsl-telecom-crm/telecom/telecomService/thirdg/doSuspend" method="post">
				<fieldset>
					<legend>Please enter the customer mdn for suspending:</legend>
					<p>
						<label>MDN</label>
						<input type="text" name="mdn" value=""/>
					</p>
					<p>
						<input type="submit" value="Suspend"/>
					</p>
				</fieldset>
			</form>
			<c:out value="${suspendResult}"></c:out><br/>
		</section>
	</body>
</html>