<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
	<head>
		<title>IDSL Telecom - ThirdG Service</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	</head>
	<body>
		<h3>Welcome to the thirdg service management page:</h3>
		<section>
			<h5>Please choose the business option:</h5>
			Provision Service <input type="button" value="provision" onclick="location.href='/idsl-telecom-crm/telecom/telecomService/thirdg/provision';" /><br><br>
			List all customer <input type="button" value="list" onclick="location.href='/idsl-telecom-crm/telecom/telecomService/thirdg/queryForAllCustomer';" /><br><br>
			Query for specific customer <input type="button" value="query" onclick="location.href='/idsl-telecom-crm/telecom/telecomService/thirdg/queryForSpecificCustomer';" /><br><br>
			Suspend Customer <input type="button" value="suspend" onclick="location.href='/idsl-telecom-crm/telecom/telecomService/thirdg/suspend';" /><br><br>
			Reactivate Customer <input type="button" value="reactivate" onclick="location.href='/idsl-telecom-crm/telecom/telecomService/thirdg/reactivate';" /><br><br>
		</section>
	</body>
</html>