<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
	<head>
		<title>IDSL Telecom - ThirdG Service Customer List</title>
	</head>
	<body>
		<h2>Customer List</h2>
		<h5><a href="/idsl-telecom-crm/telecom/telecomService/thirdgBackup/doQueryForAllCustomer">Click to list all the customer:</a></h5>
		<br/>
		<br/>
		<table border="1px">
			<tr>
				<td>Customer ID</td>
				<td>Customer Name</td>
				<td>MDN</td>
				<td>Prov Status</td>
				<td>Voice Fee</td>
				<td>Mobile Fee</td>
				<td>Contract Type</td>
				<td>Effected Date</td>
				<td>Termination Date</td>
			</tr>
			
			<c:forEach items="${customerList}" var="customerRecordForList">
				<tr>
					<td>${customerRecordForList.customerId}</td>
					<td>${customerRecordForList.customerName}</td>
					<td>${customerRecordForList.mdn}</td>
					<td>${customerRecordForList.provStatus}</td>
					<td>${customerRecordForList.voiceFee}</td>
					<td>${customerRecordForList.mobileFee}</td>
					<td>${customerRecordForList.contractType}</td>
					<td>${customerRecordForList.effectedDate}</td>
					<td>${customerRecordForList.terminationDate}</td>
				</tr>
			</c:forEach>
		</table>
	</body>
</html>