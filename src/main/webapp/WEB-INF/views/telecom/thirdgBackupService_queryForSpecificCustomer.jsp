<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
	<head>
		<title>IDSL Telecom - ThirdG Service For Query Specific Customer</title>
	</head>
	<body>
		<h2>Customer Data</h2>
		<h5>Enter the mdn to query the customer data:</a></h5><br/><br/>
		<form action="/idsl-telecom-crm/telecom/telecomService/thirdgBackup/doQueryForSpecificCustomer" method="post">
				MDN     <input type="text" name="mdn" value=""/><br/>
				<input type="submit" value="Query"/>
		</form>
		<br/><br/>
		<table border="1px">
			<tr>
				<td>Customer ID</td>
				<td>Customer Name</td>
				<td>Age</td>
				<td>Gender</td>
				<td>Birthday</td>
				<td>Address</td>
				<td>Contact Phone</td>
			</tr>
			<tr>
				<td>${customer.customerId}</td>
				<td>${customer.customerName}</td>
				<td>${customer.age}</td>
				<td>${customer.gender}</td>
				<td>${customer.birthday}</td>
				<td>${customer.address}</td>
				<td>${customer.contactPhone}</td>
			</tr>
		</table>
		<br><br>
		<table border="1px">
			<tr>
				<td>MDN</td>
				<td>Provision Status</td>
				<td>Voice Rate ID</td>
				<td>Voice Fee</td>
				<td>Mobile Rate ID</td>
				<td>Mobile Fee</td>
				<td>Contract Type</td>
				<td>Effected Date</td>
				<td>Termination Date</td>
			</tr>
			<tr>
				<td>${customer.mdnForThirdg}</td>
				<td>${customer.provStatusForThirdg}</td>
				<td>${customer.voiceRateIdForThirdg}</td>
				<td>${customer.voiceFeeForThirdg}</td>
				<td>${customer.mobileRateIdForThirdg}</td>
				<td>${customer.mobileFeeForThirdg}</td>
				<td>${customer.contractTypeForThirdg}</td>
				<td>${customer.effectedDateForThirdg}</td>
				<td>${customer.terminationDateForThirdg}</td>
			</tr>
		</table>
	</body>
</html>