<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
	<head>
		<title>IDSL Telecom - CRBT Service Page</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	</head>
	<body>
		<h2>CRBT Service:</h2>
		<section>
			<legend>Current Music List:</legend>
			<table border="1px">
				<tr>
					<td>Song Number</td>
					<td>Name</td>
					<td>Language</td>
				</tr>
				<c:forEach items="${songList}" var="music">
					<tr>
						<td>${music.songNumber}</td>
						<td>${music.name}</td>
						<td>${music.language}</td>
					</tr>
				</c:forEach>
			</table>
			<br/><br/><br/>
		</section>
		<section>
			<form action="/idsl-telecom-crm/telecom/telecomService/value_added/crbt/setCRBTForSpecificCustomer" method="post">
				<fieldset>
					<legend>Please set the specific song for the customer:</legend>
					<p>
						<label>MDN</label>
						<input type="text" name="mdn" value=""/>
					</p>
					<p>
						<label>Song Number</label>
						<input type="text" name="songNumber" value=""/>
					</p>
					<p>
						<input type="submit" value="Set CRBT"/>
					</p>
				</fieldset>
			</form>
			<c:out value="${setCRBTResult}"></c:out><br/><br/><br/>
		</section>
		<section>
			<form action="/idsl-telecom-crm/telecom/telecomService/value_added/crbt/showCRBTForSpecificCustomer" method="post">
				<fieldset>
					<legend>Please enter the customer mdn to query the CRBT information:</legend>
					<p>
						<label>MDN</label>
						<input type="text" name="mdnForQuery" value=""/>
					</p>
					<p>
						<input type="submit" value="Query CRBT"/>
					</p>
				</fieldset>
			</form>
			<table border="1px">
				<tr>
					<td>Song Number</td>
					<td>Name</td>
					<td>Language</td>
				</tr>
				<tr>
					<td>${queryResult.songNumber}</td>
					<td>${queryResult.name}</td>
					<td>${queryResult.language}</td>
				</tr>
			</table><br/><br/><br/>
		</section>
	</body>
</html>