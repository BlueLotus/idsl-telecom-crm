<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
	<head>
		<title>IDSL Telecom - ThirdG Service Reactivate Page</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	</head>
	<body>
		<h2>ThirdG reactivate for customer:</h2>
		<section>
			<form action="/idsl-telecom-crm/telecom/telecomService/thirdg/doReactivate" method="post">
				<fieldset>
					<legend>Please enter the customer mdn for reactivating:</legend>
					<p>
						<label>MDN</label>
						<input type="text" name="mdn" value=""/>
					</p>
					<p>
						<input type="submit" value="Reactivate"/>
					</p>
				</fieldset>
			</form>
			<c:out value="${reactivateResult}"></c:out><br/>
		</section>
	</body>
</html>