<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
	<head>
		<title>Spring MVC Greeting Page</title>
	</head>
<body>
	
	<section>
		<h2>Bring you a more colorful mobile life!!</h2><br/><br/>
		<h4>Here are the services we offer:</h4>
		<article>
				<p>Third G Service</p>
				<p>Fourth G Service</p>
				<p>Value-added Service (CRBT)</p><br/><br/>
				<aside>
					<p>It's our pleasure to serve you.</p>
				</aside>
		</article>
	</section>

</body>
</html>