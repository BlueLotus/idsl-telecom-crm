<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
	<head>
	  <meta charset="utf-8">
	  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	  <title>IDSL Telecom Login</title>
	  <link href='<c:url value="/css/style.css"/>' rel="stylesheet" media="screen">
	  <!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	</head>
	<body>
		<h1>IDSL Telecom CRM System</h1>
		<h3>Bring you a more colorful mobile life!</h3>
		
		<form method="post" action="/idsl-telecom-crm/telecom/doLogin" class="login">
		    <p>
		        <label for="login">EmployeeID:</label>
		        <input type="text" name="userName" id="login">
		    </p>
		    <p>
		        <label for="password">Password:</label>
		        <input type="password" name="password" id="password">
		    </p>
		    <p class="login-submit">
		        <button type="submit" class="login-button">Login</button>
		    </p>
		</form>
		
		<section class="about">
		    <p class="about-links">
		        About us
		    </p>
		    <p class="about-author">
		        &copy; 2014 <a href="http://idsl.cs.ntust.edu.tw/" target="_blank">IDSL - Dep. IM - NTUST</a><br>
		</section>
	</body>
</html>